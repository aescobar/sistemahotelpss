package puc.pss.tex.obj;


import com.google.common.base.Strings;

import java.math.BigInteger;
import java.util.ArrayList;

/**
 * @author Ariel
 * @version 1.0
 * @created 10-Oct-2013 3:34:08 AM
 */
public class Pessoa {

    private BigInteger codPessoa;
    private String nome;
    private String sobreNome;
    private String eMail;
    private Dominio pais;
    private String telefono;
    private String cidade;
    private String endereco;
    private String cpf;
    private String docId;

    public Pessoa() {
         pais = new Dominio();
    }

    public void finalize() throws Throwable {

    }

    public BigInteger getCodPessoa() {
        return codPessoa;
    }

    /**
     * @param newVal
     */
    public void setCodPessoa(BigInteger newVal) {
        codPessoa = newVal;
    }

    public String getSobreNome() {
        return sobreNome;
    }

    /**
     * @param newVal
     */
    public void setSobreNome(String newVal) {
        sobreNome = newVal;
    }

    public String getNome() {
        return nome;
    }

    /**
     * @param newVal
     */
    public void setNome(String newVal) {
        nome = newVal;
    }

    public String getTelefono() {
        return telefono;
    }

    /**
     * @param newVal
     */
    public void setTelefono(String newVal) {
        telefono = newVal;
    }

    public String getEMail() {
        return eMail;
    }

    /**
     * @param newVal
     */
    public void setEMail(String newVal) {
        eMail = newVal;
    }

    public Dominio getPais() {
        return pais;
    }

    /**
     * @param newVal
     */
    public void setPais(Dominio newVal) {
        pais = newVal;
    }

    public String getCidade() {
        return cidade;
    }

    /**
     * @param newVal
     */
    public void setCidade(String newVal) {
        cidade = newVal;
    }

    public String getEndereco() {
        return endereco;
    }

    /**
     * @param newVal
     */
    public void setEndereco(String newVal) {
        endereco = newVal;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public ArrayList<String> validar(){
        ArrayList<String> errores = new ArrayList<String>();
        if(Strings.isNullOrEmpty(nome))
            errores.add("Precisa cadastrar o nome do cliente");
        if(Strings.isNullOrEmpty(telefono))
            errores.add("Precisa cadastrar o telefone do cliente");
        if(pais==null )
            errores.add("Precisa cadastrar o pais");
        else if(Strings.isNullOrEmpty(pais.getValor()))
            errores.add("Precisa cadastrar o pais");
        return errores;
    }
}