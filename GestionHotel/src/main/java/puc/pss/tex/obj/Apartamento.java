package puc.pss.tex.obj;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author Ariel
 * @version 1.0
 * @created 10-Oct-2013 3:34:05 AM
 */
public class Apartamento {

	private BigInteger codApartamento;
	private String numero;
	private String descricao;
	private BigDecimal costo;
	private Dominio tipo;
    private Integer capacidade;

	public Apartamento(){

	}

    public BigInteger getCodApartamento() {
        return codApartamento;
    }

    public void setCodApartamento(BigInteger codApartamento) {
        this.codApartamento = codApartamento;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getCosto() {
        return costo;
    }

    public void setCosto(BigDecimal costo) {
        this.costo = costo;
    }

    public Dominio getTipo() {
        return tipo;
    }

    public void setTipo(Dominio tipo) {
        this.tipo = tipo;
    }

    public Integer getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(Integer capacidade) {
        this.capacidade = capacidade;
    }
}