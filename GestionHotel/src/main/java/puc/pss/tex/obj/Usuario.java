package puc.pss.tex.obj;

import java.math.BigInteger;

/**
 * @author Ariel
 * @version 1.0
 * @created 10-Oct-2013 3:34:07 AM
 */
public class Usuario extends Pessoa {

	private BigInteger codUsuario;
	private String nomeUsuario;
	private String senha;
	private Boolean ativo;

	public Usuario(){

	}

    public BigInteger getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(BigInteger codUsuario) {
        this.codUsuario = codUsuario;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}