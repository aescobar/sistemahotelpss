package puc.pss.tex.obj;

/**
 * @author Ariel
 * @version 1.0
 * @created 10-Oct-2013 3:34:10 AM
 */
public class Dominio {

	private String grupo;
	private String valor;
	private String descricao;


	public Dominio(){

	}

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}