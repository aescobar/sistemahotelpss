package puc.pss.tex.obj;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Ariel
 * @version 1.0
 * @created 10-Oct-2013 3:34:06 AM
 */
public class Reserva {

    private BigInteger codReserva;
    private Date dataReserva;
    private Date inicioReserva;
    private Date finReserva;
    private Cliente clienteReserva;
    private ArrayList<Apartamento> apartamentos;
    private String camaExtra;
    private String estado;

    public Reserva() {

    }

    public BigInteger getCodReserva() {
        return codReserva;
    }

    public void setCodReserva(BigInteger codReserva) {
        this.codReserva = codReserva;
    }

    public Date getDataReserva() {
        return dataReserva;
    }

    public void setDataReserva(Date dataReserva) {
        this.dataReserva = dataReserva;
    }

    public Date getInicioReserva() {
        return inicioReserva;
    }

    public void setInicioReserva(Date inicioReserva) {
        this.inicioReserva = inicioReserva;
    }

    public Date getFinReserva() {
        return finReserva;
    }

    public void setFinReserva(Date finReserva) {
        this.finReserva = finReserva;
    }

    public Cliente getClienteReserva() {
        return clienteReserva;
    }

    public void setClienteReserva(Cliente clienteReserva) {
        this.clienteReserva = clienteReserva;
    }

    public ArrayList<Apartamento> getApartamentos() {
        return apartamentos;
    }

    public void setApartamentos(ArrayList<Apartamento> apartamentos) {
        this.apartamentos = apartamentos;
    }

    public String getCamaExtra() {
        return camaExtra;
    }

    public void setCamaExtra(String camaExtra) {
        this.camaExtra = camaExtra;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public ArrayList<String> validarReserva() {
        ArrayList<String> errores = new ArrayList<String>();
        if (clienteReserva == null)
            errores.add("Precisa selecionar um cliente");
        else if (clienteReserva.getCodCliente() == null)
            errores.add("Precisa selecionar um cliente");
        if(apartamentos == null)
            errores.add("Precisa selecionar um apartamento");
        else if(apartamentos.size()<1)
            errores.add("Precisa selecionar um apartamento");
        else if(apartamentos.get(0)==null)
            errores.add("Precisa selecionar um apartamento");
        if(inicioReserva.compareTo(finReserva)>0)
            errores.add("Data Fim de Reserva deve ser maior que a Data Incio");
        return errores;
    }


}