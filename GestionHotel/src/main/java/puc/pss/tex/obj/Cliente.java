package puc.pss.tex.obj;

import com.google.common.base.Strings;

import java.math.BigInteger;
import java.util.ArrayList;

/**
 * @author Ariel
 * @version 1.0
 * @created 10-Oct-2013 3:34:04 AM
 */
public class Cliente extends Pessoa {

	private BigInteger codCliente;

	public Cliente(){

	}

    public BigInteger getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(BigInteger codCliente) {
        this.codCliente = codCliente;
    }

    public ArrayList<String> validar(){
        return super.validar();
    }
}