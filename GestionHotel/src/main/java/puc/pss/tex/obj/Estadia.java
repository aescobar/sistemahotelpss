package puc.pss.tex.obj;

import puc.pss.tex.common.Util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Ariel
 * @version 1.0
 * @created 10-Oct-2013 3:34:09 AM
 */
public class Estadia {

    private BigInteger codEstadia;
    private Cliente clienteTitular;
    private Apartamento apartamento;
    private Date dataInicio;
    private Date dataFin;
    private BigDecimal costo;
    private String observacao;
    private Integer numeroPessoas;
    private String estado;
    private BigInteger codReserva;
    private String camaExtra;

    public Estadia() {

    }

    public BigInteger getCodEstadia() {
        return codEstadia;
    }

    public void setCodEstadia(BigInteger codEstadia) {
        this.codEstadia = codEstadia;
    }

    public Cliente getClienteTitular() {
        return clienteTitular;
    }

    public void setClienteTitular(Cliente clienteTitular) {
        this.clienteTitular = clienteTitular;
    }

    public Apartamento getApartamento() {
        return apartamento;
    }

    public void setApartamento(Apartamento apartamento) {
        this.apartamento = apartamento;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFin() {
        return dataFin;
    }

    public void setDataFin(Date dataFin) {
        this.dataFin = dataFin;
    }

    public BigDecimal getCosto() {
        return costo;
    }

    public void setCosto(BigDecimal costo) {
        this.costo = costo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Integer getNumeroPessoas() {
        return numeroPessoas;
    }

    public void setNumeroPessoas(Integer numeroPessoas) {
        this.numeroPessoas = numeroPessoas;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public BigInteger getCodReserva() {
        return codReserva;
    }

    public void setCodReserva(BigInteger codReserva) {
        this.codReserva = codReserva;
    }

    public String getCamaExtra() {
        return camaExtra;
    }

    public void setCamaExtra(String camaExtra) {
        this.camaExtra = camaExtra;
    }

    public ArrayList<String> validarEstadia() {
        ArrayList<String> errores = new ArrayList<String>();
        if (clienteTitular == null)
            errores.add("Precisa selecionar um cliente");
        else if (clienteTitular.getCodCliente() == null)
            errores.add("Precisa selecionar um cliente");
        if (apartamento == null)
            errores.add("Precisa selecionar um apartamento");
        else {
            if (apartamento.getCodApartamento() == null)
                errores.add("Precisa selecionar um apartamento");
            if (numeroPessoas != null){
                if (numeroPessoas > apartamento.getCapacidade())
                    if (camaExtra.equalsIgnoreCase("S")) {
                        if (numeroPessoas > apartamento.getCapacidade() + 1)
                            errores.add("Número de pessoas não pode ser maior que capacidade de apartamento");
                    } else
                        errores.add("Número de pessoas não pode ser maior que capacidade de apartamento");
            }else
                errores.add("Precisa cadastrar o número de pessoas que moraran no apartamento");
        }
        if (dataInicio.compareTo(dataFin) > 0)
            errores.add("Data Fim de Estadia deve ser maior que a Data Incio");
        return errores;
    }

    public Integer calcularDias(){
        return Util.diferenciaDias(dataInicio, dataFin);
    }

    public BigDecimal calcularCostoEstadia(){
        BigDecimal costoTotal = apartamento.getCosto().multiply(new BigDecimal(calcularDias()));
        if (camaExtra.equalsIgnoreCase("S")) {
            costoTotal = new BigDecimal(costoTotal.longValue()+(costoTotal.doubleValue() * 0.3)).setScale(2, RoundingMode.HALF_UP);
        }
        this.costo = costoTotal;
        return costoTotal;
    }
}