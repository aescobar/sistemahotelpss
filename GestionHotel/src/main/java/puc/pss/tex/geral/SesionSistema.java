package puc.pss.tex.geral;

import puc.pss.tex.obj.Estadia;
import puc.pss.tex.obj.Reserva;
import puc.pss.tex.obj.Usuario;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * Date: 10/10/13
 * Time: 6:35 AM
 */
public class SesionSistema {
    public final static String S_USUARIO = "usuario";
    public final static String S_RESERVASELECTED= "reserva_selected";
    public final static String S_ESTADIAACTION= "estadia_action";
    public final static String S_ESTADIASELECTED= "estadia_selected";
    public enum actionEstadia{
        reservaAEstadia,
        novaEstadia,
        atualizarEstadia,
        checkOutEstadia,
    }

    public static Usuario getUsuarioActual() {
        try {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            return (Usuario) session.getAttribute(SesionSistema.S_USUARIO);
        } catch (Exception e) {
            return new Usuario();
        }
    }

    public static Reserva getReservaActual() {
        try {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            return (Reserva) session.getAttribute(SesionSistema.S_RESERVASELECTED);
        } catch (Exception e) {
            return null;
        }
    }

    public static Estadia getEstadiaActual() {
        try {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            return (Estadia) session.getAttribute(SesionSistema.S_ESTADIASELECTED);
        } catch (Exception e) {
            return null;
        }
    }

    public static actionEstadia getActionestadia() {
        try {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            return (actionEstadia) session.getAttribute(SesionSistema.S_ESTADIAACTION);
        } catch (Exception e) {
            return null;
        }
    }
}
