package puc.pss.tex.bd;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import puc.pss.tex.common.Util;
import puc.pss.tex.obj.Estadia;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 *
 * @version 1.0
 * @created 10-Oct-2013 3:34:17 AM
 */
public class EstadiaDB implements IEstadiaDB {

    QueryRunner run = new QueryRunner(ConnectDB.getDataSource());
    IClienteDB clienteDB = new ClienteDB();
    IApartamentoDB apartamentoDB = new ApartamentoDB();

    public EstadiaDB() {

    }

    @Override
    public ArrayList<Estadia> getEstadiaByCodCliente(BigInteger codCliente) throws SQLException {
        ArrayList<Estadia> lstEstadias = new ArrayList<Estadia>();

        String query = "SELECT cod_estadia, cod_cliente, cod_apartamento, data_inicio, data_fin, \n" +
                "       costo, observacao, numero_pesoas, estado, cod_reserva, cama_extra\n" +
                "  FROM estadias\n" +
                "  WHERE cod_cliente = " + codCliente;
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler());

        for (Map<String, Object> item : resp) {
            Estadia itemEstadia = new Estadia();
            itemEstadia.setCodEstadia(item.get("cod_estadia") != null ?
                    new BigInteger(item.get("cod_estadia").toString()) : null);
            itemEstadia.setCodReserva(item.get("cod_reserva") != null ?
                    new BigInteger(item.get("cod_reserva").toString()) : null);
            itemEstadia.setDataInicio(item.get("data_inicio") != null ?
                    Util.stringToDate(item.get("data_inicio").toString()) : null);
            itemEstadia.setDataFin(item.get("data_fin") != null ?
                    Util.stringToDate(item.get("data_fin").toString()) : null);
            itemEstadia.setEstado((String) item.get("estado"));
            itemEstadia.setCamaExtra((String) item.get("cama_extra"));
            itemEstadia.setNumeroPessoas(item.get("numero_pesoas") != null ?
                    new Integer(item.get("numero_pesoas").toString()) : null);
            itemEstadia.setCosto(item.get("costo") != null ?
                    new BigDecimal(item.get("costo").toString()) : null);
            itemEstadia.setObservacao((String) item.get("observacao"));
            itemEstadia.setClienteTitular(clienteDB.getClienteByCod(codCliente));
            BigInteger codApartamento = item.get("cod_apartamento") != null ?
                    new BigInteger(item.get("cod_apartamento").toString()) : null;
            itemEstadia.setApartamento(apartamentoDB.getApartamentoByCod(codApartamento));
            lstEstadias.add(itemEstadia);
        }
        return lstEstadias;
    }

    @Override
    public BigInteger insertEstadia(Estadia estadia) throws SQLException {

        String query = "INSERT INTO estadias(cod_cliente, cod_apartamento, data_inicio, data_fin,\n" +
                "                costo, observacao, numero_pesoas, estado, cod_reserva, cama_extra)\n" +
                "        VALUES (" + estadia.getClienteTitular().getCodCliente() + ", " + estadia.getApartamento().getCodApartamento() + "," +
                "?, ?, ?, ?, ?, ?, " + estadia.getCodReserva() + ",?) RETURNING cod_estadia";
        String codReserva = run.query(query, new ScalarHandler<Number>(), new java.sql.Date(estadia.getDataInicio().getTime()),
                new java.sql.Date(estadia.getDataFin().getTime()), estadia.getCosto(),
                estadia.getObservacao(), estadia.getNumeroPessoas(), estadia.getEstado(), estadia.getCamaExtra()).toString();
        BigInteger cod = new BigInteger(codReserva);
        return cod;
    }

    @Override
    public void updateEstadia(Estadia estadia) throws SQLException {

        String query = "UPDATE estadias\n" +
                "        SET  cod_cliente=" + estadia.getClienteTitular().getCodCliente() + ", " +
                "        cod_apartamento=" + estadia.getApartamento().getCodApartamento() + ", data_inicio=?,\n" +
                "        data_fin=?, costo=?, observacao=?, numero_pesoas=?, estado=?, " +
                "cod_reserva =" + estadia.getCodReserva() + ", cama_extra = ?" +
                "        WHERE cod_estadia=" + estadia.getCodEstadia();
        run.update(query, new java.sql.Date(estadia.getDataInicio().getTime()),
                new java.sql.Date(estadia.getDataFin().getTime()), estadia.getCosto(),
                estadia.getObservacao(), estadia.getNumeroPessoas(), estadia.getEstado(), estadia.getCamaExtra());

    }

    @Override
    public ArrayList<Estadia> getEstadiasActivas() throws SQLException {
        ArrayList<Estadia> lstEstadias = new ArrayList<Estadia>();

        String query = "SELECT e.cod_estadia, e.cod_cliente, e.cod_apartamento, e.data_inicio, e.data_fin, \n" +
                "       e.costo, e.observacao, e.numero_pesoas, e.estado, e.cod_reserva, e.cama_extra\n" +
                "  FROM estadias e\n" +
                "  WHERE e.estado = 'AC' ORDER BY e.data_inicio";
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler());

        for (Map<String, Object> item : resp) {
            Estadia itemEstadia = new Estadia();
            itemEstadia.setCodEstadia(item.get("cod_estadia") != null ?
                    new BigInteger(item.get("cod_estadia").toString()) : null);
            itemEstadia.setCodReserva(item.get("cod_reserva") != null ?
                    new BigInteger(item.get("cod_reserva").toString()) : null);
            itemEstadia.setDataInicio(item.get("data_inicio") != null ?
                    Util.stringToDate(item.get("data_inicio").toString()) : null);
            itemEstadia.setDataFin(item.get("data_fin") != null ?
                    Util.stringToDate(item.get("data_fin").toString()) : null);
            itemEstadia.setEstado((String) item.get("estado"));
            itemEstadia.setCamaExtra((String) item.get("cama_extra"));
            itemEstadia.setNumeroPessoas(item.get("numero_pesoas") != null ?
                    new Integer(item.get("numero_pesoas").toString()) : null);
            itemEstadia.setCosto(item.get("costo") != null ?
                    new BigDecimal(item.get("costo").toString()) : null);
            itemEstadia.setObservacao((String) item.get("observacao"));
            BigInteger codCliente = item.get("cod_cliente") != null ?
                    new BigInteger(item.get("cod_cliente").toString()) : null;
            itemEstadia.setClienteTitular(clienteDB.getClienteByCod(codCliente));
            BigInteger codApartamento = item.get("cod_apartamento") != null ?
                    new BigInteger(item.get("cod_apartamento").toString()) : null;
            itemEstadia.setApartamento(apartamentoDB.getApartamentoByCod(codApartamento));
            lstEstadias.add(itemEstadia);
        }
        return lstEstadias;
    }

    @Override
    public ArrayList<Estadia> getEstadiasByCodApartamento(BigInteger codApartamento) throws SQLException {
        ArrayList<Estadia> lstEstadias = new ArrayList<Estadia>();

        String query = "SELECT cod_estadia, cod_cliente, cod_apartamento, data_inicio, data_fin, \n" +
                "       costo, observacao, numero_pesoas, estado, cod_reserva, cama_extra\n" +
                "  FROM estadias\n" +
                "  WHERE cod_apartamento = " + codApartamento;
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler());

        for (Map<String, Object> item : resp) {
            Estadia itemEstadia = new Estadia();
            itemEstadia.setCodEstadia(item.get("cod_estadia") != null ?
                    new BigInteger(item.get("cod_estadia").toString()) : null);
            itemEstadia.setCodReserva(item.get("cod_reserva") != null ?
                    new BigInteger(item.get("cod_reserva").toString()) : null);
            itemEstadia.setDataInicio(item.get("data_inicio") != null ?
                    Util.stringToDate(item.get("data_inicio").toString()) : null);
            itemEstadia.setDataFin(item.get("data_fin") != null ?
                    Util.stringToDate(item.get("data_fin").toString()) : null);
            itemEstadia.setEstado((String) item.get("estado"));
            itemEstadia.setCamaExtra((String) item.get("cama_extra"));
            itemEstadia.setNumeroPessoas(item.get("numero_pesoas") != null ?
                    new Integer(item.get("numero_pesoas").toString()) : null);
            itemEstadia.setCosto(item.get("costo") != null ?
                    new BigDecimal(item.get("costo").toString()) : null);
            itemEstadia.setObservacao((String) item.get("observacao"));
            BigInteger codCliente = item.get("cod_cliente") != null ?
                    new BigInteger(item.get("cod_cliente").toString()) : null;
            itemEstadia.setClienteTitular(clienteDB.getClienteByCod(codCliente));
            itemEstadia.setApartamento(apartamentoDB.getApartamentoByCod(codApartamento));
            lstEstadias.add(itemEstadia);
        }
        return lstEstadias;
    }

    @Override
    public Integer countEstadiasAtivas() throws SQLException {
        String query = "SELECT count(1) FROM estadias WHERE estado = 'AC' ";
        Integer numeroEstadias = Integer.parseInt(run.query(query, new ScalarHandler<Number>()).toString());
        return numeroEstadias;
    }

    @Override
    public BigDecimal calculateCosto(Date dataInicio, Date dataFim) throws SQLException {
        String query = "SELECT sum(e.costo) FROM estadias e WHERE e.estado = 'FN' AND e.data_fin BETWEEN ? AND ?";
        Object resp = run.query(query, new ScalarHandler<Number>(), new java.sql.Date(dataInicio.getTime())
                , new java.sql.Date(dataFim.getTime()));
        if (resp != null)
            return new BigDecimal(resp.toString());
        else
            return BigDecimal.ZERO;
    }

    @Override
    public Integer calculatePessoas(Date dataInicio, Date dataFim) throws SQLException {
        String query = "SELECT sum(e.numero_pesoas) FROM estadias e WHERE e.data_fin BETWEEN ? AND ?";
        Object resp = run.query(query, new ScalarHandler<Number>(), new java.sql.Date(dataInicio.getTime())
                , new java.sql.Date(dataFim.getTime()));
        if (resp != null)
            return Integer.parseInt(resp.toString());
        else
            return 0;
    }


    public static void main(String[] args) throws SQLException {
        EstadiaDB b = new EstadiaDB();
        System.out.println(b.countEstadiasAtivas());
        System.out.println(b.calculateCosto(Util.primerDia(new Date()), Util.ultimoDia(new Date())));
        System.out.println(b.calculatePessoas(Util.primerDia(new Date()), Util.ultimoDia(new Date())));
        ReservaDB a = new ReservaDB();
        System.out.println(a.countReservas(new Date()));
    }
}