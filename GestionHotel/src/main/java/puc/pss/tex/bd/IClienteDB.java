package puc.pss.tex.bd;

import puc.pss.tex.obj.Cliente;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * @version 1.0
 * @created 10-Oct-2013 3:34:23 AM
 */
public interface IClienteDB {

	public Cliente getClienteByCod(BigInteger codCliente) throws SQLException;

	public void updateCliente(Cliente cliente);

	public BigInteger insertCliente(Cliente pessoa) throws SQLException;

	public ArrayList<Cliente> findCliente(Cliente cliente) throws SQLException;

}