package puc.pss.tex.bd;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import puc.pss.tex.common.Util;
import puc.pss.tex.obj.Apartamento;
import puc.pss.tex.obj.Cliente;
import puc.pss.tex.obj.Dominio;
import puc.pss.tex.obj.Reserva;

/**
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * @version 1.0
 * @created 10-Oct-2013 3:34:11 AM
 */
public class ApartamentoDB implements IApartamentoDB {

    QueryRunner run = new QueryRunner(ConnectDB.getDataSource());

	public ApartamentoDB(){

	}

    @Override
    public Apartamento getApartamentoByCod(BigInteger codApartamento) throws SQLException {
        String query = "SELECT a.cod_apartamento, a.numero, a.descricao, a.costo, a.tipo, a.capacidade, d.descricao\n" +
                "  FROM apartamentos a, dominios d\n" +
                "  WHERE a.cod_apartamento = " +  codApartamento + "AND d.grupo = 'TIPO_APARTAMENTO' " +
                "  AND d.valor = a.tipo";
        Map<String, Object> resp;
        resp = run.query(query, new MapHandler() );
        Apartamento itemApartamento = null;
        if(resp != null) {
            itemApartamento = new Apartamento();
            itemApartamento.setCodApartamento(resp.get("cod_apartamento") != null ?
                    new BigInteger(resp.get("cod_apartamento").toString()) : null);
            itemApartamento.setNumero((String) resp.get("numero"));
            itemApartamento.setDescricao((String) resp.get("descricao"));
            itemApartamento.setCosto(resp.get("costo") != null ?
                    new BigDecimal(resp.get("costo").toString()) : BigDecimal.ZERO);
            Dominio objDominio = new Dominio();
            objDominio.setValor((String) resp.get("tipo"));
            objDominio.setDescricao((String) resp.get("descricao"));
            itemApartamento.setTipo(objDominio);
            itemApartamento.setCapacidade(resp.get("capacidade") != null ?
                    new Integer(resp.get("capacidade").toString()) : 0);
        }
        return itemApartamento;
    }

    @Override
    public ArrayList<Apartamento> findApartamento(String numero) throws SQLException {
        ArrayList<Apartamento> lstApartamentos = new ArrayList<Apartamento>();

        String query = "SELECT cod_apartamento, numero, descricao, costo, tipo, capacidade\n" +
                "  FROM apartamentos\n" +
                "  WHERE numero = ?";
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler(), numero);

        for (Map<String, Object> item : resp) {
            Apartamento itemApartamento = new Apartamento();
            itemApartamento.setCodApartamento(item.get("cod_apartamento") != null ?
                    new BigInteger(item.get("cod_apartamento").toString()) : null);
            itemApartamento.setNumero((String) item.get("numero"));
            itemApartamento.setDescricao((String) item.get("descricao"));
            itemApartamento.setCosto(item.get("costo") != null ?
                    new BigDecimal(item.get("costo").toString()) : BigDecimal.ZERO);
            Dominio objDominio = new Dominio();
            objDominio.setValor((String) item.get("tipo"));
            itemApartamento.setTipo(objDominio);
            itemApartamento.setCapacidade(item.get("capacidade") != null ?
                    new Integer(item.get("capacidade").toString()) : 0);
            lstApartamentos.add(itemApartamento);
        }
        return lstApartamentos;
    }

    @Override
    public Integer countApartamentos() throws SQLException {
        String query = "SELECT count(1) FROM apartamentos a";
        Integer codReserva = Integer.parseInt(run.query(query, new ScalarHandler<Number>()).toString());
        return codReserva;
    }

    public static void main(String[] args) throws SQLException {
        ApartamentoDB b = new ApartamentoDB();
        System.out.println(b.countApartamentos());

    }

    @Override
    public ArrayList<Apartamento> getApartamentosLivres(Date dataInicio, Date dataFin, String tipo) throws SQLException {
        ArrayList<Apartamento> lstApartamentos = new ArrayList<Apartamento>();

        String query = "SELECT a.cod_apartamento, a.numero, a.descricao, a.costo, a.tipo, a.capacidade\n" +
                "  FROM apartamentos a\n" +
                "  WHERE a.cod_apartamento NOT IN (SELECT ap.cod_apartamento\n" +
                "  FROM apartamentos ap, reserva_apartamentos ra, reservas r\n" +
                "  WHERE r.estado = 'PE' AND ap.cod_apartamento = ra.cod_apartamento AND r.cod_reserva = ra.cod_reserva\n" +
                "  AND ((? >= r.inicio_reserva AND ? < r.fin_reserva)\n" +
                "  OR (? > r.inicio_reserva AND ? <= r.fin_reserva))\t\n" +
                "  AND (r.inicio_reserva >= current_date)) \n" +
                "  AND a.cod_apartamento NOT IN (SELECT ap.cod_apartamento\n" +
                "  FROM apartamentos ap, estadias e\n" +
                "  WHERE e.estado = 'AC' AND ap.cod_apartamento = e.cod_apartamento \n" +
                "  AND ((? >= e.data_inicio AND ? < e.data_fin )\n" +
                "  OR (? > e.data_inicio AND ? <= e.data_fin))\t\n" +
                "  AND (e.data_inicio>= current_date)) AND a.tipo = ?";
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler(), new java.sql.Date(dataInicio.getTime()),
                new java.sql.Date(dataInicio.getTime()), new java.sql.Date(dataFin.getTime()),
                new java.sql.Date(dataFin.getTime()), new java.sql.Date(dataInicio.getTime()),
                new java.sql.Date(dataInicio.getTime()), new java.sql.Date(dataFin.getTime()),
                new java.sql.Date(dataFin.getTime()), tipo );

        for (Map<String, Object> item : resp) {
            Apartamento itemApartamento = new Apartamento();
            itemApartamento.setCodApartamento(item.get("cod_apartamento") != null ?
                    new BigInteger(item.get("cod_apartamento").toString()) : null);
            itemApartamento.setNumero((String) item.get("numero"));
            itemApartamento.setDescricao((String) item.get("descricao"));
            itemApartamento.setCosto(item.get("costo") != null ?
                    new BigDecimal(item.get("costo").toString()) : BigDecimal.ZERO);
            Dominio objDominio = new Dominio();
            objDominio.setValor((String) item.get("tipo"));
            itemApartamento.setTipo(objDominio);
            itemApartamento.setCapacidade(item.get("capacidade") != null ?
                    new Integer(item.get("capacidade").toString()) : 0);
            lstApartamentos.add(itemApartamento);
        }
        return lstApartamentos;
    }
}