package puc.pss.tex.bd;

import puc.pss.tex.obj.Usuario;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * @version 1.0
 * @created 10-Oct-2013 3:34:18 AM
 */
public interface IUsuarioDB {

	public Usuario getUsuarioByCod(BigInteger codUsuario);

	public Usuario loginUsuario(String senha, String nomeUsuario) throws SQLException;

}