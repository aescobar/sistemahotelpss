package puc.pss.tex.bd;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import puc.pss.tex.common.Util;
import puc.pss.tex.obj.Apartamento;
import puc.pss.tex.obj.Cliente;
import puc.pss.tex.obj.Dominio;
import puc.pss.tex.obj.Reserva;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 *
 * @version 1.0
 * @created 10-Oct-2013 3:34:15 AM
 */
public class DominioDB implements IDominioDB {

    QueryRunner run = new QueryRunner(ConnectDB.getDataSource());

    public DominioDB() {

    }

    @Override
    public ArrayList<Dominio> getDominiosByGrupo(String grupo) throws SQLException {
        ArrayList<Dominio> lstDominios = new ArrayList<Dominio>();

        String query = "SELECT * FROM DOMINIOS WHERE grupo = ?";
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler(), grupo);

        for (Map<String, Object> item : resp) {
            Dominio itemDominio = new Dominio();
            itemDominio.setGrupo((String) item.get("grupo"));
            itemDominio.setValor((String) item.get("valor"));
            itemDominio.setDescricao((String) item.get("descricao"));
            lstDominios.add(itemDominio);
        }
        return lstDominios;
    }

    @Override
    public Dominio getDominioByGrupoValor(String valor, String grupo) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}