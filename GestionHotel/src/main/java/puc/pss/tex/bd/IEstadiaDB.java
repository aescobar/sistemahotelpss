package puc.pss.tex.bd;

import puc.pss.tex.obj.Estadia;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 *
 * @version 1.0
 * @created 10-Oct-2013 3:34:16 AM
 */
public interface IEstadiaDB {

    public ArrayList<Estadia> getEstadiaByCodCliente(BigInteger codCliente) throws SQLException;

    public BigInteger insertEstadia(Estadia estadia) throws SQLException;

    public void updateEstadia(Estadia estadia) throws SQLException;

    public ArrayList<Estadia> getEstadiasByCodApartamento(BigInteger codApartamento) throws SQLException;

    ArrayList<Estadia> getEstadiasActivas() throws SQLException;

    Integer countEstadiasAtivas() throws SQLException;

    BigDecimal calculateCosto(Date dataInicio, Date dataFim) throws SQLException;

    Integer calculatePessoas(Date dataInicio, Date dataFim) throws SQLException;
}