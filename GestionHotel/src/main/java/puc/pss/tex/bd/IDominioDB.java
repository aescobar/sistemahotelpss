package puc.pss.tex.bd;

import puc.pss.tex.obj.Dominio;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * @version 1.0
 * @created 10-Oct-2013 3:34:13 AM
 */
public interface IDominioDB {

	public ArrayList<Dominio> getDominiosByGrupo(String grupo) throws SQLException;

	public Dominio getDominioByGrupoValor(String valor, String grupo);

}