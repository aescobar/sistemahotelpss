package puc.pss.tex.bd;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import puc.pss.tex.obj.Dominio;
import puc.pss.tex.obj.Usuario;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

/**
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 *
 * @version 1.0
 * @created 10-Oct-2013 3:34:19 AM
 */
public class UsuarioDB implements IUsuarioDB {

    QueryRunner run = new QueryRunner(ConnectDB.getDataSource());

    public UsuarioDB() {

    }

    @Override
    public Usuario getUsuarioByCod(BigInteger codUsuario) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Usuario loginUsuario(String senha, String nomeUsuario) throws SQLException {

        String query = "SELECT p.cod_pessoa, p.nome, p.sobre_nome, p.email, p.pais, p.cidade, p.endereco, \n" +
                "p.telefone, p.cpf, p.doc_id, u.cod_usuario, u.nome_usuario, u.senha, d.valor AS valor_pais, d.descricao AS descricao_pais  \n" +
                "FROM usuarios u, pessoas p, dominios d \n" +
                "WHERE u.cod_pessoa = p.cod_pessoa AND d.grupo = 'PAISES' AND p.pais = d.valor AND " +
                "u.nome_usuario = ? AND u.senha = ?";
        Map<String, Object> resp;
        resp = run.query(query, new MapHandler(), nomeUsuario, senha);
        Usuario objUsuario = null;
        if (resp != null && !resp.isEmpty()) {
            objUsuario = new Usuario();
            objUsuario.setCodUsuario(resp.get("cod_usuario") != null ?
                    new BigInteger(resp.get("cod_usuario").toString()) : null);
            objUsuario.setNome((String) resp.get("nome"));
            objUsuario.setSobreNome((String) resp.get("sobre_nome"));
            objUsuario.setEMail((String) resp.get("email"));
            Dominio objDominio = new Dominio();
            objDominio.setValor((String) resp.get("valor_pais"));
            objDominio.setDescricao((String) resp.get("descricao_pais"));
            objUsuario.setPais(objDominio);
            objUsuario.setCidade((String) resp.get("cidade"));
            objUsuario.setEndereco((String) resp.get("endereco"));
            objUsuario.setTelefono((String) resp.get("telefone"));
            objUsuario.setCpf((String) resp.get("cpf"));
            objUsuario.setDocId((String) resp.get("doc_id"));
            objUsuario.setCodPessoa(resp.get("cod_pessoa") != null ?
                    new BigInteger(resp.get("cod_pessoa").toString()) : null);
        }
        return objUsuario;
    }

}