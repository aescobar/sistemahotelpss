package puc.pss.tex.bd;

import com.google.common.base.Strings;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import puc.pss.tex.obj.Cliente;
import puc.pss.tex.obj.Dominio;
import puc.pss.tex.obj.Usuario;
import puc.pss.tex.obj.Pessoa;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 *
 * @version 1.0
 * @created 10-Oct-2013 3:34:22 AM
 */
public class ClienteDB implements IClienteDB {

    QueryRunner run = new QueryRunner(ConnectDB.getDataSource());


    public ClienteDB() {

    }

    @Override
    public Cliente getClienteByCod(BigInteger codCliente) throws SQLException {
        String query = "SELECT p.cod_pessoa, p.nome, p.sobre_nome, p.email, p.pais, p.cidade, p.endereco, \n" +
                "p.telefone, p.cpf, p.doc_id, c.cod_cliente, d.valor AS valor_pais, d.descricao AS descricao_pais  \n" +
                "FROM clientes c, pessoas p, dominios d \n" +
                "WHERE c.cod_pessoa = p.cod_pessoa AND d.grupo = 'PAISES' AND p.pais = d.valor AND " +
                "c.cod_cliente = "+codCliente;
        Map<String, Object> resp;
        resp = run.query(query, new MapHandler());
        Cliente objCliente = null;
        if (resp != null && !resp.isEmpty()) {
            objCliente = new Cliente();
            objCliente.setCodCliente(resp.get("cod_cliente") != null ?
                    new BigInteger(resp.get("cod_cliente").toString()) : null);
            objCliente.setNome((String) resp.get("nome"));
            objCliente.setSobreNome((String) resp.get("sobre_nome"));
            objCliente.setEMail((String) resp.get("email"));
            Dominio objDominio = new Dominio();
            objDominio.setValor((String) resp.get("valor_pais"));
            objDominio.setDescricao((String) resp.get("descricao_pais"));
            objCliente.setPais(objDominio);
            objCliente.setCidade((String) resp.get("cidade"));
            objCliente.setEndereco((String) resp.get("endereco"));
            objCliente.setTelefono((String) resp.get("telefone"));
            objCliente.setCpf((String) resp.get("cpf"));
            objCliente.setDocId((String) resp.get("doc_id"));
            objCliente.setCodPessoa(resp.get("cod_pessoa") != null ?
                    new BigInteger(resp.get("cod_pessoa").toString()) : null);
        }
        return objCliente;
    }

    @Override
    public void updateCliente(Cliente cliente) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public BigInteger insertCliente(Cliente pessoa) throws SQLException {
        String query = "INSERT INTO pessoas(\n" +
                "    nome, sobre_nome, email, cidade, endereco, telefone, \n" +
                "    cpf, doc_id, pais)\n" +
                "    VALUES (?, ?, ?, ?, ?, ?, \n" +
                "    ?, ?, ?) RETURNING cod_pessoa ;";
        String codMensaje = run.query(query, new ScalarHandler<Number>(), pessoa.getNome(), pessoa.getSobreNome(),
                pessoa.getEMail(), pessoa.getCidade(), pessoa.getEndereco(), pessoa.getTelefono(),
                pessoa.getCpf(), pessoa.getDocId(), pessoa.getPais().getValor()).toString();
        BigInteger codPessoa = new BigInteger(codMensaje);
        String query2 = "INSERT INTO clientes(cod_pessoa)\n" +
                "    VALUES ("+codPessoa+") RETURNING cod_cliente";
        String codCliente = run.query(query2, new ScalarHandler<Number>()).toString();
        return new BigInteger(codCliente);
    }

    @Override
    public ArrayList<Cliente> findCliente(Cliente cliente) throws SQLException {
        ArrayList<Cliente> lstClientes = new ArrayList<Cliente>();
        String query = "SELECT p.cod_pessoa, p.nome, p.sobre_nome, p.email, p.pais, p.cidade, p.endereco, \n" +
                "                p.telefone, p.cpf, p.doc_id, c.cod_cliente, d.valor AS valor_pais, d.descricao AS descricao_pais  \n" +
                "                FROM clientes c, pessoas p, dominios d \n" +
                "                WHERE c.cod_pessoa = p.cod_pessoa AND d.grupo = 'PAISES' AND p.pais = d.valor";
        if (cliente.getCodCliente() != null)
            query += " AND c.cod_cliente = " + cliente.getCodCliente();
        if (!Strings.isNullOrEmpty(cliente.getNome()))
            query += " AND UPPER(nome) like UPPER('%" + cliente.getNome() + "%') ";
        if (!Strings.isNullOrEmpty(cliente.getSobreNome()))
            query += " AND UPPER(sobre_nome) like UPPER('%" + cliente.getSobreNome() + "%')";
        if (!Strings.isNullOrEmpty(cliente.getCpf()))
            query += " AND cpf like '%" + cliente.getCpf() + "%'";
        if (!Strings.isNullOrEmpty(cliente.getDocId()))
            query += "AND doc_id like '%" + cliente.getDocId() + "%')) ";

        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler());

        for (Map<String, Object> itemResp : resp) {
            Cliente objCliente = new Cliente();
            objCliente.setCodCliente(itemResp.get("cod_cliente") != null ?
                    new BigInteger(itemResp.get("cod_cliente").toString()) : null);
            objCliente.setNome((String) itemResp.get("nome"));
            objCliente.setSobreNome((String) itemResp.get("sobre_nome"));
            objCliente.setEMail((String) itemResp.get("email"));
            Dominio objDominio = new Dominio();
            objDominio.setValor((String) itemResp.get("valor_pais"));
            objDominio.setDescricao((String) itemResp.get("descricao_pais"));
            objCliente.setPais(objDominio);
            objCliente.setCidade((String) itemResp.get("cidade"));
            objCliente.setEndereco((String) itemResp.get("endereco"));
            objCliente.setTelefono((String) itemResp.get("telefone"));
            objCliente.setCpf((String) itemResp.get("cpf"));
            objCliente.setDocId((String) itemResp.get("doc_id"));
            objCliente.setCodPessoa(itemResp.get("cod_pessoa") != null ?
                    new BigInteger(itemResp.get("cod_pessoa").toString()) : null);
            lstClientes.add(objCliente);
        }
        return lstClientes;
    }
}