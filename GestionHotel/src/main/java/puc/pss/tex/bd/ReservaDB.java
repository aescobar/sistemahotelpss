package puc.pss.tex.bd;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import puc.pss.tex.common.Util;
import puc.pss.tex.obj.Apartamento;
import puc.pss.tex.obj.Cliente;
import puc.pss.tex.obj.Reserva;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 *
 * @version 1.0
 * @created 10-Oct-2013 3:34:24 AM
 */
public class ReservaDB implements IReservaDB {

    QueryRunner run = new QueryRunner(ConnectDB.getDataSource());
    IClienteDB clienteDB = new ClienteDB();
    IApartamentoDB apartamentoDB = new ApartamentoDB();

    public ReservaDB() {

    }


    @Override
    public Reserva getReservaByCod(BigInteger codReserva) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public BigInteger insertReserva(Reserva reserva) throws SQLException {
        String query = "INSERT INTO reservas(\n" +
                "            data_reserva, inicio_reserva, fin_reserva, cod_cliente, \n" +
                "            cama_extra, estado)\n" +
                "    VALUES (?, ?, ?, "+reserva.getClienteReserva().getCodCliente()+", \n" +
                "            ?, ?) RETURNING cod_reserva ";
        String codReserva = run.query(query, new ScalarHandler<Number>(), new java.sql.Date(reserva.getDataReserva().getTime()),
                new java.sql.Date(reserva.getInicioReserva().getTime()), new java.sql.Date(reserva.getFinReserva().getTime()),
                 reserva.getCamaExtra(), reserva.getEstado()).toString();
        BigInteger cod = new BigInteger(codReserva);
        String query2 = "INSERT INTO reserva_apartamentos(\n" +
                "                cod_reserva, cod_apartamento)\n" +
                "        VALUES ("+cod+", "+reserva.getApartamentos().get(0).getCodApartamento()+")";
         run.update(query2);
        return cod;
    }

    @Override
    public void updateReserva(Reserva reserva) throws SQLException {

        String query = "UPDATE reservas\n" +
                "        SET data_reserva=?, inicio_reserva=?, fin_reserva=?,\n" +
                "        cod_cliente="+reserva.getClienteReserva().getCodCliente()+", cama_extra=?, estado=?\n" +
                "        WHERE cod_reserva="+reserva.getCodReserva();
        run.update(query, new java.sql.Date(reserva.getDataReserva().getTime()),
                new java.sql.Date(reserva.getInicioReserva().getTime()), new java.sql.Date(reserva.getFinReserva().getTime()),
                reserva.getCamaExtra(), reserva.getEstado());
    }

    @Override
    public ArrayList<Reserva> getReservasByCodCliente(BigInteger codCliente) throws SQLException {
        ArrayList<Reserva> lstEstadias = new ArrayList<Reserva>();

        String query = "SELECT r.cod_reserva, r.data_reserva, r.inicio_reserva, r.fin_reserva, r.cod_cliente, \n" +
                "       r.cama_extra, r.estado, ra.cod_apartamento\n" +
                "  FROM reservas r INNER JOIN reserva_apartamentos ra ON r.cod_reserva = ra.cod_reserva   \n" +
                "  WHERE r.cod_cliente = " + codCliente;
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler());

        for (Map<String, Object> item : resp) {
            Reserva itemReserva = new Reserva();
            itemReserva.setCodReserva(item.get("cod_reserva") != null ?
                    new BigInteger(item.get("cod_reserva").toString()) : null);
            itemReserva.setDataReserva(item.get("data_reserva") != null ?
                    Util.stringToDate(item.get("data_reserva").toString()) : null);
            itemReserva.setInicioReserva(item.get("inicio_reserva") != null ?
                    Util.stringToDate(item.get("inicio_reserva").toString()) : null);
            itemReserva.setFinReserva(item.get("fin_reserva") != null ?
                    Util.stringToDate(item.get("fin_reserva").toString()) : null);
            itemReserva.setEstado((String) item.get("estado"));
            itemReserva.setCamaExtra((String) item.get("cama_extra"));
            itemReserva.setClienteReserva(clienteDB.getClienteByCod(codCliente));

            ArrayList<Apartamento> apartamentos = new ArrayList<Apartamento>();
            BigInteger codApartamento= item.get("cod_apartamento") != null ?
                    new BigInteger(item.get("cod_apartamento").toString()) : null;
            apartamentos.add(apartamentoDB.getApartamentoByCod(codApartamento));
            itemReserva.setApartamentos(apartamentos);
            lstEstadias.add(itemReserva);
        }
        return lstEstadias;
    }

    @Override
    public ArrayList<Reserva> getReservasByCodApartamento(BigInteger codApartamento) throws SQLException {
        ArrayList<Reserva> lstEstadias = new ArrayList<Reserva>();

        String query = "SELECT r.cod_reserva, r.data_reserva, r.inicio_reserva, r.fin_reserva, r.cod_cliente, \n" +
                "       r.cama_extra, r.estado, ra.cod_apartamento\n" +
                "  FROM reservas r INNER JOIN reserva_apartamentos ra ON r.cod_reserva = ra.cod_reserva   \n" +
                "  WHERE ra.cod_apartamento = " + codApartamento;
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler());

        for (Map<String, Object> item : resp) {
            Reserva itemReserva = new Reserva();
            itemReserva.setCodReserva(item.get("cod_reserva") != null ?
                    new BigInteger(item.get("cod_reserva").toString()) : null);
            itemReserva.setDataReserva(item.get("data_reserva") != null ?
                    Util.stringToDate(item.get("data_reserva").toString()) : null);
            itemReserva.setInicioReserva(item.get("inicio_reserva") != null ?
                    Util.stringToDate(item.get("inicio_reserva").toString()) : null);
            itemReserva.setFinReserva(item.get("fin_reserva") != null ?
                    Util.stringToDate(item.get("fin_reserva").toString()) : null);
            itemReserva.setEstado((String) item.get("estado"));
            itemReserva.setCamaExtra((String) item.get("cama_extra"));
            BigInteger codCliente = item.get("cod_cliente") != null ?
                    new BigInteger(item.get("cod_cliente").toString()) : null;
            itemReserva.setClienteReserva(clienteDB.getClienteByCod(codCliente));

            ArrayList<Apartamento> apartamentos = new ArrayList<Apartamento>();
            apartamentos.add(apartamentoDB.getApartamentoByCod(codApartamento));
            itemReserva.setApartamentos(apartamentos);
            lstEstadias.add(itemReserva);
        }
        return lstEstadias;
    }

    @Override
    public ArrayList<Reserva> getReservasActivas() throws SQLException {
        ArrayList<Reserva> lstReservas = new ArrayList<Reserva>();

        String query = "SELECT r.cod_reserva, r.data_reserva, r.inicio_reserva, r.fin_reserva, r.estado, \n" +
                "  r.cama_extra, c.cod_cliente, p.nome, p.sobre_nome, a.numero, a.tipo, a.cod_apartamento  \n" +
                "  FROM reservas r, clientes c, pessoas p, apartamentos a, reserva_apartamentos ra\n" +
                "  WHERE r.cod_cliente = c.cod_cliente AND c.cod_pessoa = p.cod_pessoa AND r.inicio_reserva >= current_date\n" +
                "  AND ra.cod_reserva = r.cod_reserva AND ra.cod_apartamento = a.cod_apartamento AND r.estado = 'PE' ORDER BY r.inicio_reserva";
        List<Map<String, Object>> resp;
        resp = run.query(query, new MapListHandler());

        for (Map<String, Object> item : resp) {
            Reserva itemReserva = new Reserva();
            itemReserva.setCodReserva(item.get("cod_reserva") != null ?
                    new BigInteger(item.get("cod_reserva").toString()) : null);
            itemReserva.setDataReserva(item.get("data_reserva") != null ?
                    Util.stringToDate(item.get("data_reserva").toString()) : null);
            itemReserva.setInicioReserva(item.get("inicio_reserva") != null ?
                    Util.stringToDate(item.get("inicio_reserva").toString()) : null);
            itemReserva.setFinReserva(item.get("fin_reserva") != null ?
                    Util.stringToDate(item.get("fin_reserva").toString()) : null);
            itemReserva.setEstado((String) item.get("estado"));
            itemReserva.setCamaExtra((String) item.get("cama_extra"));
            Cliente objCliente = new Cliente();
            objCliente.setCodCliente(item.get("cod_cliente") != null ?
                    new BigInteger(item.get("cod_cliente").toString()) : null);
            objCliente.setNome((String) item.get("nome"));
            objCliente.setSobreNome((String) item.get("sobre_nome"));
            itemReserva.setClienteReserva(objCliente);
            Apartamento objApartamento = new Apartamento();
            objApartamento.setCodApartamento(item.get("cod_apartamento") != null ?
                    new BigInteger(item.get("cod_apartamento").toString()) : null);
            objApartamento.setNumero((String) item.get("numero"));
            ArrayList<Apartamento> lstAp = new ArrayList<Apartamento>();
            lstAp.add(objApartamento);
            itemReserva.setApartamentos(lstAp);
            lstReservas.add(itemReserva);
        }
        return lstReservas;
    }

    @Override
    public Integer countReservas(Date data) throws SQLException {
        String query = "SELECT count(1) FROM reservas WHERE inicio_reserva = ?";
        Integer numeroReservasAtuales = Integer.parseInt(run.query(query, new ScalarHandler<Number>(),
                new java.sql.Date(data.getTime())).toString());
        return numeroReservasAtuales;
    }
}