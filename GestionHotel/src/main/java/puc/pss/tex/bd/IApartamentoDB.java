package puc.pss.tex.bd;

import puc.pss.tex.obj.Apartamento;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * @version 1.0
 * @created 10-Oct-2013 3:34:13 AM
 */
public interface IApartamentoDB {

	public Apartamento getApartamentoByCod(BigInteger codApartamento) throws SQLException;

	public ArrayList<Apartamento> findApartamento(String numero) throws SQLException;

    ArrayList<Apartamento> getApartamentosLivres(Date dataInicio, Date dataFin, String tipo) throws SQLException;

    Integer countApartamentos() throws SQLException;
}