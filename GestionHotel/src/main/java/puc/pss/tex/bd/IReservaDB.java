package puc.pss.tex.bd;

import puc.pss.tex.obj.Reserva;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * @version 1.0
 * @created 10-Oct-2013 3:34:25 AM
 */
public interface IReservaDB {

	public Reserva getReservaByCod(BigInteger codReserva);

	public BigInteger insertReserva(Reserva reserva) throws SQLException;

	public void updateReserva(Reserva reserva) throws SQLException;

	public ArrayList<Reserva> getReservasByCodCliente(BigInteger codCliente) throws SQLException;

    ArrayList<Reserva> getReservasActivas() throws SQLException;

    ArrayList<Reserva> getReservasByCodApartamento(BigInteger codApartamento) throws SQLException;

    Integer countReservas(Date data) throws SQLException;
}