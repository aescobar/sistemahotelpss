package puc.pss.tex.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.Days;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar
 * Date: 5/31/13
 * Time: 1:01 AM
 */
public class Util {

    public static Date stringToDate(String fechaText) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return df.parse(fechaText);
        } catch (ParseException ex) {
            return null;
        }
    }

    public static Date stringToShortDate(String fechaText) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return df.parse(fechaText);
        } catch (ParseException ex) {
            return null;
        }
    }

    public static String dateToStringShortWebApp(Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        if (fecha != null)
            return sdf.format(fecha);
        else
            return "-";
    }

    public static String dateToStringLargeWebApp(Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        if (fecha != null)
            return sdf.format(fecha);
        else
            return "-";
    }

    public static int diferenciaDias(Date dataInicio, Date dataFin) {
        return Days.daysBetween(new DateTime(dataInicio), new DateTime(dataFin)).getDays();
    }

    public static Date primerDia(Date data) {
        Calendar c = Calendar.getInstance();
        c.setTime(data);
        c.set(Calendar.DATE, 1);
        return new Date(c.getTimeInMillis());
    }

    public static Date ultimoDia(Date data) {
        Calendar c = Calendar.getInstance();
        c.setTime(data);
        c.add(Calendar.MONTH, 1);
        c.set(Calendar.DATE, 1);
        c.add(Calendar.DATE, -1);
        return new Date(c.getTimeInMillis());
    }

    public static boolean dataActual(Date data) {
        int dias = Days.daysBetween(new DateTime(new Date()), new DateTime(data)).getDays();
        if (dias == 0)
            return true;
        else
            return false;
    }

    public static void main(String Args[]) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DATE, 5);
        c.set(Calendar.MONTH, 1);
        c.set(Calendar.YEAR, 2013);

        System.out.println(Util.primerDia(new Date(c.getTimeInMillis())));
        System.out.println(Util.ultimoDia(new Date(c.getTimeInMillis())));
    }

}
