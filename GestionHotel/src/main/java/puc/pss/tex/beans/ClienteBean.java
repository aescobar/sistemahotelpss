package puc.pss.tex.beans;

import org.apache.log4j.Logger;
import puc.pss.tex.bd.*;
import puc.pss.tex.obj.Cliente;
import puc.pss.tex.obj.Dominio;
import puc.pss.tex.obj.Estadia;
import puc.pss.tex.obj.Reserva;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * Date: 10/9/13
 * Time: 12:27 AM
 */
@ManagedBean(name = "ClienteBean")
@ViewScoped
public class ClienteBean extends BaseBean {

    private final IClienteDB clienteDB = new ClienteDB();
    private final IReservaDB reservaDB = new ReservaDB();
    private final IEstadiaDB estadiaDB = new EstadiaDB();
    private final IDominioDB dominioDB = new DominioDB();

    static final Logger LOGGER = Logger.getLogger(ClienteBean.class);

    private ArrayList<Cliente> lstClientes;
    private ArrayList<SelectItem> lstPaises;
    private Cliente objClienteProcura;
    private Cliente objClienteSelect;
    private Cliente objClienteCadastroSelect;
    private String paisSelect;
    private boolean vCadastroCliente = false;

    private boolean vRelatorioCliente = false;
    private ArrayList<Reserva> lstReservas;
    private ArrayList<Estadia> lstEstadias;

    public ClienteBean() {
        objClienteCadastroSelect = new Cliente();
    }

    public Cliente getObjClienteCadastroSelect() {
        return objClienteCadastroSelect;
    }

    public void setObjClienteCadastroSelect(Cliente objClienteCadastroSelect) {
        this.objClienteCadastroSelect = objClienteCadastroSelect;
    }

    public String getPaisSelect() {
        return paisSelect;
    }

    public void setPaisSelect(String paisSelect) {
        this.paisSelect = paisSelect;
    }

    public boolean isvRelatorioCliente() {
        return vRelatorioCliente;
    }

    public void setvRelatorioCliente(boolean vRelatorioCliente) {
        this.vRelatorioCliente = vRelatorioCliente;
    }


    public ArrayList<Cliente> getLstClientes() {
        return lstClientes;
    }

    public void setLstClientes(ArrayList<Cliente> lstClientes) {
        this.lstClientes = lstClientes;
    }

    public Cliente getObjClienteProcura() {
        if (objClienteProcura == null)
            objClienteProcura = new Cliente();
        return objClienteProcura;
    }

    public void setObjClienteProcura(Cliente objClienteProcura) {
        this.objClienteProcura = objClienteProcura;
    }

    public Cliente getObjClienteSelect() {
        if (objClienteSelect == null)
            objClienteSelect = new Cliente();
        return objClienteSelect;
    }

    public void setObjClienteSelect(Cliente objClienteSelect) {
        this.objClienteSelect = objClienteSelect;
    }

    public ArrayList<SelectItem> getLstPaises() {
        if (lstPaises == null || lstPaises.size() < 1) {
            try {
                lstPaises = new ArrayList<SelectItem>();
                ArrayList<Dominio> lstPaisesDom = dominioDB.getDominiosByGrupo("PAISES");
                for (Dominio itemDominio : lstPaisesDom) {
                    SelectItem i = new SelectItem(itemDominio.getValor(), itemDominio.getDescricao());
                    lstPaises.add(i);
                }
            } catch (SQLException e) {
                lstPaises = new ArrayList<SelectItem>();
            }
        }
        return lstPaises;
    }

    public void setLstPaises(ArrayList<SelectItem> lstPaises) {
        this.lstPaises = lstPaises;
    }

    public ArrayList<Reserva> getLstReservas() {
        return lstReservas;
    }

    public void setLstReservas(ArrayList<Reserva> lstReservas) {
        this.lstReservas = lstReservas;
    }

    public ArrayList<Estadia> getLstEstadias() {
        return lstEstadias;
    }

    public void setLstEstadias(ArrayList<Estadia> lstEstadias) {
        this.lstEstadias = lstEstadias;
    }

    public void procurarClientes() {
        try {
            lstClientes = clienteDB.findCliente(objClienteProcura);
            vRelatorioCliente = false;
        } catch (SQLException e) {
            LOGGER.error("Error al obtener clientes", e);
        }
    }

    public boolean isvCadastroCliente() {
        return vCadastroCliente;
    }

    public void setvCadastroCliente(boolean vCadastroCliente) {
        this.vCadastroCliente = vCadastroCliente;
    }

    public void selecionarCliente(Cliente objCLiente) {
        objClienteSelect = objCLiente;
        try {
            lstReservas = reservaDB.getReservasByCodCliente(objCLiente.getCodCliente());
        } catch (SQLException e) {
            LOGGER.error("Erro obter reservas do client", e);
            lstReservas = new ArrayList<Reserva>();
        }
        try {
            lstEstadias = estadiaDB.getEstadiaByCodCliente(objCLiente.getCodCliente());
        } catch (SQLException e) {
            LOGGER.error("Erro obter estadias do client", e);
            lstEstadias = new ArrayList<Estadia>();
        }
        lstClientes = new ArrayList<Cliente>();
        vRelatorioCliente = true;
    }

    public void openCadastro() {
        objClienteCadastroSelect = new Cliente();
        vCadastroCliente = true;
    }

    public void closeCadastro() {
        objClienteCadastroSelect = new Cliente();
        vCadastroCliente = false;
    }

    public void cadastrarCLiente() {
        try {
            Dominio objDOminio = new Dominio();
            objDOminio.setValor(paisSelect);
            objClienteCadastroSelect.setPais(objDOminio);
            ArrayList<String> validacion = objClienteCadastroSelect.validar();
            if (validacion.size() > 0) {
                for (String error : validacion)
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            error, error));
            } else {
                clienteDB.insertCliente(objClienteCadastroSelect);
                vCadastroCliente = false;
            }
        } catch (SQLException e) {
            LOGGER.error("Error insertar cliente");
        }
    }


}
