package puc.pss.tex.beans;

import puc.pss.tex.bd.*;
import puc.pss.tex.common.Util;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * Date: 10/21/13
 * Time: 11:59 PM
 */
@ManagedBean(name = "HotelBean")
@ViewScoped
public class HotelBean extends BaseBean{
    private IApartamentoDB apartamentoDB = new ApartamentoDB();
    private IEstadiaDB estadiaDB = new EstadiaDB();
    private IReservaDB reservaDB = new ReservaDB();
    private Integer numeroApartamentos = 0;
    private Integer numeroApartamentosOcupados = 0;
    private Integer numeroHospedes = 0;
    private Integer numeroReservas = 0;
    private BigDecimal costoMes = BigDecimal.ZERO;
    private Date dataInicio = Util.primerDia(new Date());
    private Date dataFim= Util.ultimoDia(new Date());

    public HotelBean() {
        procurarDatos();
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public Integer getNumeroApartamentos() {
        return numeroApartamentos;
    }

    public void setNumeroApartamentos(Integer numeroApartamentos) {
        this.numeroApartamentos = numeroApartamentos;
    }

    public Integer getNumeroApartamentosOcupados() {
        return numeroApartamentosOcupados;
    }

    public void setNumeroApartamentosOcupados(Integer numeroApartamentosOcupados) {
        this.numeroApartamentosOcupados = numeroApartamentosOcupados;
    }

    public Integer getNumeroHospedes() {
        return numeroHospedes;
    }

    public void setNumeroHospedes(Integer numeroHospedes) {
        this.numeroHospedes = numeroHospedes;
    }

    public Integer getNumeroReservas() {
        return numeroReservas;
    }

    public void setNumeroReservas(Integer numeroReservas) {
        this.numeroReservas = numeroReservas;
    }

    public BigDecimal getCostoMes() {
        return costoMes;
    }

    public void setCostoMes(BigDecimal costoMes) {
        this.costoMes = costoMes;
    }

    public void procurarDatos(){
        try {
            numeroApartamentos = apartamentoDB.countApartamentos();
        } catch (SQLException e) {
            numeroApartamentos = 0;
        }
        try {
            numeroApartamentosOcupados = estadiaDB.countEstadiasAtivas();
        } catch (SQLException e) {
            numeroApartamentosOcupados = 0;
        }
        try {
            numeroHospedes = estadiaDB.calculatePessoas(Util.primerDia(new Date()), Util.ultimoDia(new Date()));
        } catch (SQLException e) {
            numeroHospedes = 0;
        }
        try {
            numeroReservas = reservaDB.countReservas(new Date());
        } catch (SQLException e) {
            numeroReservas = 0;
        }
        try {
            costoMes = estadiaDB.calculateCosto(Util.primerDia(new Date()), Util.ultimoDia(new Date()));
        } catch (SQLException e) {
            costoMes = BigDecimal.ZERO;
        }
    }
}
