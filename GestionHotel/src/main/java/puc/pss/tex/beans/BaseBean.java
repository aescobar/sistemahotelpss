package puc.pss.tex.beans;


import puc.pss.tex.common.Util;

import java.util.Calendar;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar
 * Company: MC4 S.R.L.
 * Date: 7/2/13
 * Time: 3:40 PM
 */
public class BaseBean {
    private String mensajeTablaVacia = "No existe registros.";
    private String minDataValue = "";

    public String getMensajeTablaVacia() {
        return mensajeTablaVacia;
    }

    public void setMensajeTablaVacia(String mensajeTablaVacia) {
        this.mensajeTablaVacia = mensajeTablaVacia;
    }

    public String dateToStringLarge(Date fecha){
        return Util.dateToStringLargeWebApp(fecha);
    }

    public String dateToStringShort(Date fecha){
        return Util.dateToStringShortWebApp(fecha);
    }

    public String getMinDataValue() {
        return Util.dateToStringShortWebApp(new Date());
    }

    public String getMinDataValue(Date data) {
        Calendar c = Calendar.getInstance();
        c.setTime(data);
        c.add(Calendar.DATE, 1);
        data = c.getTime();
        return Util.dateToStringShortWebApp(data);
    }

    public void setMinDataValue(String minDataValue) {
        this.minDataValue = minDataValue;
    }

    public boolean dataActual(Date data){
        return Util.dataActual(data);
    }
}
