package puc.pss.tex.beans;

import org.apache.log4j.Logger;
import puc.pss.tex.bd.IReservaDB;
import puc.pss.tex.bd.ReservaDB;
import puc.pss.tex.geral.SesionSistema;
import puc.pss.tex.obj.Reserva;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * Date: 10/9/13
 * Time: 12:27 AM
 */
@ManagedBean(name = "ReservasBean")
@ViewScoped
public class ReservasBean extends BaseBean {

    private IReservaDB reservaDB = new ReservaDB();

    static final Logger LOGGER = Logger.getLogger(ReservasBean.class);

    private ArrayList<Reserva> lstReservas;
    private ArrayList<Reserva> lstReservasFilter;
    private  Reserva reservaSelected;

    public ReservasBean() {
        lstReservas = new ArrayList<Reserva>();
    }

    public ArrayList<Reserva> getLstReservas() {
        try {
            lstReservas = reservaDB.getReservasActivas();
        } catch (SQLException e) {
            lstReservas = new ArrayList<Reserva>();
        }
        return lstReservas;
    }

    public void setLstReservas(ArrayList<Reserva> lstReservas) {
        this.lstReservas = lstReservas;
    }

    public ArrayList<Reserva> getLstReservasFilter() {
        return lstReservasFilter;
    }

    public void setLstReservasFilter(ArrayList<Reserva> lstReservasFilter) {
        this.lstReservasFilter = lstReservasFilter;
    }

    public Reserva getReservaSelected() {
        return reservaSelected;
    }

    public void setReservaSelected(Reserva reservaSelected) {
        this.reservaSelected = reservaSelected;
    }

    public void nuevaReserva() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext extContext = context.getExternalContext();
            String url;
            url = extContext.encodeActionURL(context.getApplication().getViewHandler().getActionURL(context, "/adm/registroReserva.jsf"));
            extContext.redirect(url);
        } catch (IOException e) {
            LOGGER.error("Error al redireccionar pagina");
        }
    }

    public void nuevaEstadia(Reserva reservaAct) {
        try {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            session.setAttribute(SesionSistema.S_RESERVASELECTED, reservaAct);
            session.setAttribute(SesionSistema.S_ESTADIAACTION, SesionSistema.actionEstadia.reservaAEstadia);
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext extContext = context.getExternalContext();
            String url = "";
            url = extContext.encodeActionURL(context.getApplication().getViewHandler().getActionURL(context, "/adm/registroEstadia.jsf"));
            extContext.redirect(url);
        } catch (IOException e) {
            LOGGER.error("Error al redireccionar pagina");
        }
    }
}
