package puc.pss.tex.beans;

import org.apache.log4j.Logger;
import puc.pss.tex.bd.*;
import puc.pss.tex.obj.*;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * Date: 10/9/13
 * Time: 12:27 AM
 */
@ManagedBean(name = "ApartamentoBean")
@ViewScoped
public class ApartamentoBean extends BaseBean {

    private final IApartamentoDB apartamentoDB = new ApartamentoDB();
    private final IReservaDB reservaDB = new ReservaDB();
    private final IEstadiaDB estadiaDB = new EstadiaDB();

    static final Logger LOGGER = Logger.getLogger(ApartamentoBean.class);

    private String numeroProcura;
    private ArrayList<Apartamento> lstApartamentos;
    private Cliente objClienteSelect;
    private Apartamento objApartamentoSelect;

    private boolean vRelatorioCliente = false;
    private ArrayList<Reserva> lstReservas;
    private ArrayList<Estadia> lstEstadias;

    public ApartamentoBean() {
    }

    public Apartamento getObjApartamentoSelect() {
        return objApartamentoSelect;
    }

    public void setObjApartamentoSelect(Apartamento objApartamentoSelect) {
        this.objApartamentoSelect = objApartamentoSelect;
    }

    public String getNumeroProcura() {
        return numeroProcura;
    }

    public void setNumeroProcura(String numeroProcura) {
        this.numeroProcura = numeroProcura;
    }

    public boolean isvRelatorioCliente() {
        return vRelatorioCliente;
    }

    public void setvRelatorioCliente(boolean vRelatorioCliente) {
        this.vRelatorioCliente = vRelatorioCliente;
    }

    public ArrayList<Reserva> getLstReservas() {
        return lstReservas;
    }

    public void setLstReservas(ArrayList<Reserva> lstReservas) {
        this.lstReservas = lstReservas;
    }

    public ArrayList<Estadia> getLstEstadias() {
        return lstEstadias;
    }

    public void setLstEstadias(ArrayList<Estadia> lstEstadias) {
        this.lstEstadias = lstEstadias;
    }

    public ArrayList<Apartamento> getLstApartamentos() {
        return lstApartamentos;
    }

    public void setLstApartamentos(ArrayList<Apartamento> lstApartamentos) {
        this.lstApartamentos = lstApartamentos;
    }

    public void procurarApartamentos() {
        try {
            lstApartamentos = apartamentoDB.findApartamento(numeroProcura);
            vRelatorioCliente = false;
        } catch (SQLException e) {
            LOGGER.error("Error al obtener clientes", e);
        }
    }


    public void selecionarApartamento(Apartamento objApartamento) {
        objApartamentoSelect= objApartamento;
        try {
            lstReservas = reservaDB.getReservasByCodApartamento(objApartamento.getCodApartamento());
        } catch (SQLException e) {
            LOGGER.error("Erro obter reservas do client", e);
            lstReservas = new ArrayList<Reserva>();
        }
        try {
            lstEstadias = estadiaDB.getEstadiasByCodApartamento(objApartamento.getCodApartamento());
        } catch (SQLException e) {
            LOGGER.error("Erro obter estadias do client", e);
            lstEstadias = new ArrayList<Estadia>();
        }
        lstApartamentos = new ArrayList<Apartamento>();
        vRelatorioCliente = true;
    }

}
