package puc.pss.tex.beans;

import org.apache.log4j.Logger;
import puc.pss.tex.bd.EstadiaDB;
import puc.pss.tex.bd.IEstadiaDB;
import puc.pss.tex.common.Util;
import puc.pss.tex.geral.SesionSistema;
import puc.pss.tex.obj.Estadia;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * Date: 10/9/13
 * Time: 12:27 AM
 */
@ManagedBean(name = "EstadiaBean")
@ViewScoped
public class EstadiaBean extends BaseBean {

    private IEstadiaDB estadiaDB = new EstadiaDB();

    static final Logger LOGGER = Logger.getLogger(EstadiaBean.class);

    private ArrayList<Estadia> lstEstadias;
    private ArrayList<Estadia> lstEstadiasFilter;
    private Estadia estadiaSelected;
    private BigDecimal costoTotal;
    private Integer numeroDias;

    private boolean vMensaje = false;

    public EstadiaBean() {
        lstEstadias = new ArrayList<Estadia>();
    }

    public ArrayList<Estadia> getLstEstadias() {
        try {
            lstEstadias = estadiaDB.getEstadiasActivas();
        } catch (SQLException e) {
            lstEstadias = new ArrayList<Estadia>();
            LOGGER.error("Error listar estadias", e);
        }
        return lstEstadias;
    }

    public void setLstEstadias(ArrayList<Estadia> lstEstadias) {
        this.lstEstadias = lstEstadias;
    }

    public Estadia getEstadiaSelected() {
        return estadiaSelected;
    }

    public void setEstadiaSelected(Estadia estadiaSelected) {
        this.estadiaSelected = estadiaSelected;
    }

    public ArrayList<Estadia> getLstEstadiasFilter() {
        return lstEstadiasFilter;
    }

    public void setLstEstadiasFilter(ArrayList<Estadia> lstEstadiasFilter) {
        this.lstEstadiasFilter = lstEstadiasFilter;
    }

    public BigDecimal getCostoTotal() {
        return costoTotal;
    }

    public void setCostoTotal(BigDecimal costoTotal) {
        this.costoTotal = costoTotal;
    }

    public boolean isvMensaje() {
        return vMensaje;
    }

    public void setvMensaje(boolean vMensaje) {
        this.vMensaje = vMensaje;
    }

    public Integer getNumeroDias() {
        return numeroDias;
    }

    public void setNumeroDias(Integer numeroDias) {
        this.numeroDias = numeroDias;
    }

    public void nuevaEstadia() {
        try {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            session.setAttribute(SesionSistema.S_ESTADIAACTION, SesionSistema.actionEstadia.novaEstadia);
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext extContext = context.getExternalContext();
            String url = extContext.encodeActionURL(context.getApplication().getViewHandler().getActionURL(context, "/adm/registroEstadia.jsf"));
            extContext.redirect(url);
        } catch (IOException e) {
            LOGGER.error("Error al redireccionar pagina");
        }
    }

    public void terminarEstadia(Estadia estadia) {
        try {
            estadiaSelected = estadia;
            estadia.setDataFin(new Date());
            numeroDias = estadiaSelected.calcularDias();
            costoTotal = estadiaSelected.calcularCostoEstadia();
            vMensaje = true;
        } catch (Exception e) {
            LOGGER.error("Error al mostrar check out");
        }
    }

    public void confirmCheckOut(){
        try {
            estadiaSelected.setEstado("FN");
            estadiaDB.updateEstadia(estadiaSelected);
            vMensaje = false;

        } catch (SQLException e) {
            LOGGER.error("Error check Out", e);
        }
    }
}
