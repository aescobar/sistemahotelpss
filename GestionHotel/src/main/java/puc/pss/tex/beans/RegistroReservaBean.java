package puc.pss.tex.beans;

import org.apache.log4j.Logger;
import puc.pss.tex.bd.*;
import puc.pss.tex.obj.Apartamento;
import puc.pss.tex.obj.Cliente;
import puc.pss.tex.obj.Dominio;
import puc.pss.tex.obj.Reserva;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * Date: 10/9/13
 * Time: 12:27 AM
 */
@ManagedBean(name = "RegistroReservaBean")
@ViewScoped
public class RegistroReservaBean extends BaseBean {

    private final IReservaDB reservaDB = new ReservaDB();
    private final IDominioDB dominioDB = new DominioDB();
    private final IApartamentoDB apartamentoDB = new ApartamentoDB();
    private final IClienteDB clienteDB = new ClienteDB();

    static final Logger LOGGER = Logger.getLogger(RegistroReservaBean.class);

    private ArrayList<Dominio> lstTipoApartamento;
    private Apartamento objApartamentoSelect;
    private ArrayList<Apartamento> lstApartamentosLivres;
    private ArrayList<Cliente> lstClientes;
    private Cliente objClienteProcura;
    private Cliente objClienteSelect;
    private String tipoApartamentoSelecionado = "ST";
    private Date dataInicio = new Date();
    private Date dataFin = new Date();
    private Reserva objReserva;
    private String camaExtra = "N";

    //Visible
    private boolean vApartamentosLivres = false;
    private boolean vClientes = false;
    private boolean vMensaje = false;

    public RegistroReservaBean() {
        LOGGER.info("ingreso a la pag Registro Reserva");
        dataModificada();
    }

    private void reiniciarDatos() {
        lstClientes = new ArrayList<Cliente>();
        lstApartamentosLivres = new ArrayList<Apartamento>();
        dataInicio = new Date();
        dataFin = new Date();
        tipoApartamentoSelecionado = "";
        objClienteProcura = new Cliente();
        objClienteSelect = new Cliente();
        objApartamentoSelect = new Apartamento();
        vApartamentosLivres = false;
        vClientes = false;
    }

    public ArrayList<Dominio> getLstTipoApartamento() {
        try {
            if (lstTipoApartamento == null)
                lstTipoApartamento = dominioDB.getDominiosByGrupo("TIPO_APARTAMENTO");
        } catch (SQLException e) {
            lstTipoApartamento = new ArrayList<Dominio>();
        }
        return lstTipoApartamento;
    }

    public void setLstTipoApartamento(ArrayList<Dominio> lstTipoApartamento) {
        this.lstTipoApartamento = lstTipoApartamento;
    }

    public String getTipoApartamentoSelecionado() {
        return tipoApartamentoSelecionado;
    }

    public void setTipoApartamentoSelecionado(String tipoApartamentoSelecionado) {
        this.tipoApartamentoSelecionado = tipoApartamentoSelecionado;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFin() {
        return dataFin;
    }

    public void setDataFin(Date dataFin) {
        this.dataFin = dataFin;
    }

    public ArrayList<Apartamento> getLstApartamentosLivres() {
        return lstApartamentosLivres;
    }

    public void setLstApartamentosLivres(ArrayList<Apartamento> lstApartamentosLivres) {
        this.lstApartamentosLivres = lstApartamentosLivres;
    }

    public ArrayList<Cliente> getLstClientes() {
        return lstClientes;
    }

    public void setLstClientes(ArrayList<Cliente> lstClientes) {
        this.lstClientes = lstClientes;
    }

    public Apartamento getObjApartamentoSelect() {
        return objApartamentoSelect;
    }

    public void setObjApartamentoSelect(Apartamento objApartamentoSelect) {
        this.objApartamentoSelect = objApartamentoSelect;
    }

    public Cliente getObjClienteProcura() {
        if (objClienteProcura == null)
            objClienteProcura = new Cliente();
        return objClienteProcura;
    }

    public void setObjClienteProcura(Cliente objClienteProcura) {
        this.objClienteProcura = objClienteProcura;
    }

    public Cliente getObjClienteSelect() {
        return objClienteSelect;
    }

    public void setObjClienteSelect(Cliente objClienteSelect) {
        this.objClienteSelect = objClienteSelect;
    }

    public boolean isvApartamentosLivres() {
        return vApartamentosLivres;
    }

    public void setvApartamentosLivres(boolean vApartamentosLivres) {
        this.vApartamentosLivres = vApartamentosLivres;
    }

    public boolean isvClientes() {
        return vClientes;
    }

    public void setvClientes(boolean vClientes) {
        this.vClientes = vClientes;
    }

    public String getCamaExtra() {
        return camaExtra;
    }

    public void setCamaExtra(String camaExtra) {
        this.camaExtra = camaExtra;
    }

    public Reserva getObjReserva() {
        if (objReserva == null)
            objReserva = new Reserva();
        return objReserva;
    }

    public void setObjReserva(Reserva objReserva) {
        this.objReserva = objReserva;
    }

    public boolean isvMensaje() {
        return vMensaje;
    }

    public void setvMensaje(boolean vMensaje) {
        this.vMensaje = vMensaje;
    }

    public void procurarApartamentosLivres() {
        try {
            lstApartamentosLivres = apartamentoDB.getApartamentosLivres(dataInicio, dataFin, tipoApartamentoSelecionado);
            vApartamentosLivres = true;
        } catch (SQLException e) {
            LOGGER.error("Error al obtener apartamentos livres", e);
        }
    }

    public void procurarClientes() {
        try {
            lstClientes = clienteDB.findCliente(objClienteProcura);
            vClientes = true;
        } catch (SQLException e) {
            LOGGER.error("Error al obtener clientes", e);
        }
    }

    public void fazVisibleSelecionarCliente() {
        vClientes = true;
    }

    public void selecionarCliente(Cliente objCLiente) {
        objClienteSelect = objCLiente;
        lstClientes = new ArrayList<Cliente>();
        vClientes = false;
    }

    public void selecionarApartamento(Apartamento objApartamento) {
        objApartamentoSelect = objApartamento;
        vApartamentosLivres = false;
    }

    public void dataModificada() {
        objApartamentoSelect = new Apartamento();
        if (dataFin != null) {
            if (dataFin.compareTo(dataInicio) <= 0) {
                Calendar c = Calendar.getInstance();
                c.setTime(dataInicio);
                c.add(Calendar.DATE, 1);
                dataFin = c.getTime();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Data Saida modificada automaticamente", "Data Saida modificada automaticamente"));
            }
        }

    }

    public void cadastrarReserva() {
        try {
            objReserva = new Reserva();
            ArrayList<Apartamento> lstAp = new ArrayList<Apartamento>();
            lstAp.add(objApartamentoSelect);
            objReserva.setApartamentos(lstAp);
            objReserva.setClienteReserva(objClienteSelect);
            objReserva.setInicioReserva(dataInicio);
            objReserva.setFinReserva(dataFin);
            objReserva.setDataReserva(new Date());
            objReserva.setEstado("PE");
            objReserva.setCamaExtra(camaExtra);
            ArrayList<String> validacion = objReserva.validarReserva();
            if (validacion.size() > 0) {
                for (String error : validacion)
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            error, error));
            } else {
                reservaDB.insertReserva(objReserva);
                vMensaje = true;
            }
        } catch (SQLException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Erro cadastro reserva tente depois por favor", "Erro cadastro reserva tente depois por favor"));
            LOGGER.error("Error cadastrar reserva", e);
        }
    }

    public void voltarReserva() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext extContext = context.getExternalContext();
            String url = "";
            url = extContext.encodeActionURL(context.getApplication().getViewHandler().getActionURL(context, "/adm/reserva.jsf"));
            extContext.redirect(url);
        } catch (IOException e) {
            LOGGER.error("Error al redireccionar pagina", e);
        }
    }

}
