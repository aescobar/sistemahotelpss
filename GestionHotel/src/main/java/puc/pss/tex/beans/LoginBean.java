package puc.pss.tex.beans;

import org.apache.log4j.Logger;
import puc.pss.tex.bd.UsuarioDB;
import puc.pss.tex.geral.SesionSistema;
import puc.pss.tex.obj.Usuario;
import puc.pss.tex.bd.IUsuarioDB;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigInteger;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar Endara
 * Pontifícia Universidade Católica do Rio de Janeiro
 * Date: 10/9/13
 * Time: 12:27 AM
 */
@ManagedBean(name = "LoginBean")
@ViewScoped
public class LoginBean {

   private IUsuarioDB usuarioDB = new UsuarioDB();

    static final Logger LOGGER = Logger.getLogger(LoginBean.class);

    private Usuario objUsuario;
    private String nick;
    private String password;

    public LoginBean() {
        this.objUsuario = new Usuario();
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void ingresar() {
        try {

            Usuario usuarioIngreso = usuarioDB.loginUsuario(password, nick);
            if (usuarioIngreso != null) {
                LOGGER.info(usuarioIngreso.getNomeUsuario());
                objUsuario = usuarioIngreso;
                HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                session.setAttribute(SesionSistema.S_USUARIO, usuarioIngreso);
                FacesContext context = FacesContext.getCurrentInstance();
                ExternalContext extContext = context.getExternalContext();
                String url = extContext.encodeActionURL(context.getApplication().getViewHandler().getActionURL(context, "/adm/reserva.jsf"));
                extContext.redirect(url);

            } else
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Los datos son incorrectos", "Los datos son incorrectos"));
        } catch (IOException e) {
            LOGGER.fatal("Error al redireccionar desde login", e);
        } catch (SQLException e) {
            LOGGER.fatal("Error al consultar la BD ", e);
        }
    }

    public void salir() {
        try {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            session.invalidate();
//            FacesContext contex = FacesContext.getCurrentInstance();
//            contex.getExternalContext().redirect("/login.jsf");
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext extContext = context.getExternalContext();
            String url = extContext.encodeActionURL(context.getApplication().getViewHandler().getActionURL(context, "/Login.jsf"));
            extContext.redirect(url);
        } catch (IOException e) {
            LOGGER.fatal("Error al redireccionar desde login", e);
        }
    }

    public Usuario getUsuarioActual(){
        return SesionSistema.getUsuarioActual();
    }
}
